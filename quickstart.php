<?php
require __DIR__ . '/vendor/autoload.php';

if (php_sapi_name() != 'cli') {
    throw new Exception('This application must be run on the command line.');
}

class Drive_permissions{
	public $client;
	private $credentials='credentials.json';
	public $service;
	public $user;
	public $userEmail;
	public $userFiles=array();
	public $geralEmail='geral.td@torresdistrib.com';
	private $files= array();
	private $folders= array();
	
	
	public function __construct(){
		if (!file_exists($this->credentials)) {
			printf("Ficheiro credentials.json não encontrado. criar um novo acesso OAuth 2.0 no site \n %s \n e colocar o ficheiro na raiz.\n", 'https://console.developers.google.com/apis/credentials?project=quickstart-1558442484213');
			return exit;
		}
		
		array_push($this->folders, array('id'=>'145yndRW-Z-pme8FycMI9egzrmvmXzPNP', 'nome'=>'share Torres Distrib')); //Folder inicial
		print 'Qual o user (do dominio @torresdistrib.com) que quer transferir a propriedade das fotos? ';
		$this->getUser();
		$this->service = new Google_Service_Drive($this->client);
		$this->folderCrawler();
		$this->updateFiles();

	}
	
	private function getUser(){
		$user = trim(fgets(STDIN));
		if(!$user or $user=='') {
			printf("Utilizador não válido\n");
			return exit();
		}else{
			$this->client = new Google_Client();
			$this->client->setApplicationName('Google Drive API PHP Quickstart');
			$this->client->setScopes(Google_Service_Drive::DRIVE);
			$this->client->setAuthConfig('credentials.json');
			$this->client->setAccessType('offline');
			$this->client->setPrompt('select_account consent');
			$tokenPath = 'tokens/'.$user.'.json';
			$this->userEmail=$user.'@torresdistrib.com';
			
			if (file_exists($tokenPath)) {
				$accessToken = json_decode(file_get_contents($tokenPath), true);
				$this->client->setAccessToken($accessToken);
			}
			// If there is no previous token or it's expired.
			if ($this->client->isAccessTokenExpired()) {
				// Refresh the token if possible, else fetch a new one.
				if ($this->client->getRefreshToken()) {
					$this->client->fetchAccessTokenWithRefreshToken($this->client->getRefreshToken());
				} else {
					// Request authorization from the user.
					$authUrl = $this->client->createAuthUrl();
					printf("Open the following link in your browser:\n%s\n", $authUrl);
					print 'Enter verification code: ';
					$authCode = trim(fgets(STDIN));

					// Exchange authorization code for an access token.
					$accessToken = $this->client->fetchAccessTokenWithAuthCode($authCode);
					$this->client->setAccessToken($accessToken);

					// Check to see if there was an error.
					if (array_key_exists('error', $accessToken)) {
						throw new Exception(join(', ', $accessToken));
					}
				}
				// Save the token to a file.
				if (!file_exists(dirname($tokenPath))) {
					mkdir(dirname($tokenPath), 0700, true);
				}
				file_put_contents($tokenPath, json_encode($this->client->getAccessToken()));
			}
			return $this->client;
		}
	}

public function folderCrawler(){
	printf("/////////////// FOLDER CRAWLER //////////////////////\n");
	do{	
		$folder_id=$this->folders[0]['id'];
		$folder_items=$this->listFiles($folder_id);
		printf("Nova Pasta Encontrada : %s\n", $this->folders[0]['nome']);
		array_splice($this->folders,0,1);
		foreach ($folder_items->getFiles() as $item) {
			if($item->mimeType=='application/vnd.google-apps.folder'){
				printf("	New Folder-> %s (%s)\n", $item->getName(), $item->getId());
				array_push($this->folders, array('id'=> $item->getId(), 'nome'=>$item->getName()));
			}else{
				printf("	Ficheiro encontrado-> %s (owner: %s)\n", $item->getName(), $item->owners[0]->emailAddress);
				array_push($this->files, $item->owners[0]->emailAddress);
				
				if($item->owners[0]->emailAddress == $this->userEmail)
					array_push($this->userFiles, $item);
			}
		}
	} while (count($this->folders) > 0);

	printf("\nResumo:\n");
	printf("Total ficheiros encontrados: %s\n", count($this->files));
	foreach(array_count_values($this->files) as $email=>$count){
		printf("	%s -> %s\n ", $email, $count);
	}

}
	
	public function listFiles($folder_id){
		$optParams_files = array(
		  'pageSize'=> 10,
		  'fields' 	=> 'nextPageToken, files(owners, id, mimeType, name, permissions)',
		  'q' 	=> "'".$folder_id."' in parents"
		);
		return $this->service->files->listFiles($optParams_files);
	}
	
	
	private function transferOwnership($file) {
	  try {
		$optParams = array(
		  "transferOwnership"=>true,
		);
		
		$p = $this->service->permissions->get($file->id, $file->owners[0]->permissionId);
		$newOwnerPermission = new Google_Service_Drive_Permission(array(
			'type' => 'user',
			'role' => 'owner',
			'emailAddress' => $this->geralEmail
		));
		return $this->service->permissions->create($file->id, $newOwnerPermission, $optParams);
		
	  } catch (Exception $e) {
		printf("An error occurred: %s \n", $e->getMessage());
		return false;
	  }
	  return NULL;
	}
	
	public function updateFiles(){
		if(count($this->userFiles)>0){
			printf("\n/////////////////////////////////////////\n\n");
			printf(" Actualização dos ficheiros de %s \n",$this->userEmail);
			$result_count=0;
			foreach($this->userFiles as $f){
				printf("A actualizar o ficheiro : %s    ", $f->name);
				if(!$this->transferOwnership($f)){
					printf("erro na actualização!!!!!\n");
					break;
				}

				printf("Ok!\n");
				$result_count ++;
			}
			printf("Total ficheiros actualizados: %s \n",$result_count);	
		}
	}
}




//INICIO
$drive = new Drive_permissions();
	